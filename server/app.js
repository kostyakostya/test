const express = require("express");
const cors = require("cors");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema");
const users = [
  {
    id: 1,
    fname: "First Dude",
    lname: "But Cool",
    email: "first_cooldude@mail.ru",
    age: 22,
  },
];

const app = express();
const PORT = 5000;

app.use(cors());

const createUser = (input) => {
  const id = Math.floor(Math.random() * 100);
  return {
    id,
    ...input,
  };
};

const root = {
  getAllUsers: () => {
    return users;
  },
  getUser: ({ id }) => {
    return users.find((user) => user.id == id);
  },
  createUser: ({ input }) => {
    const newUser = createUser(input);
    users.push(newUser);
    return newUser;
  },
};

app.use(
  "/graphql",
  graphqlHTTP({
    graphiql: true,
    schema,
    rootValue: root,
  })
);

const start = () => {
  try {
    app.listen(PORT, () => {
      console.log(`App has been started on port ${PORT}...`);
    });
  } catch (error) {
    console.log("error", error);
  }
};

start();
